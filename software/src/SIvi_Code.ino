//GND - GND
//VCC - VCC
//SDA - Pin 3
//SCL - Pin 2
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"


const int mpuAddress = 0x68;
MPU6050 mpu(mpuAddress);

int ax, ay, az;
int gx, gy, gz;
int modo = 0;
int Modos;
int espera;

long tiempo_prev;
float dt;
float ang_x, ang_y, ang_z;
float ang_x_prev = 0, ang_y_prev = 0, ang_z_prev = 0;
float ang_x_offset = 0, ang_y_offset = 0, ang_z_offset = 0;
bool isVibrating;

unsigned long tiempoled = 0;
unsigned long tiempoinclinacion = 0;
unsigned long tiempovibracion = 0;
unsigned long tiempo_espera;
unsigned long tiempo_esperado = 5000;

const int ledPinTilt = 5;
const int ledPinALive = 7;
const int buzzerPin = 6;
const int ledPinVibracion = 4;  // Nuevo LED para indicar vibración
const int botonPin = 8;
const int frequency1 = 1000;
const int frequency2 = 2000;
const int frec_inictalizate = 3000;
const int calibrationTime = 5000; // Calibration time
const float pond_ang = 0.5; // Ponderacion flitro comp
const float pond_flit = 0.8;

void setup() {
  Serial.begin(9600);
  Wire.begin();

  mpu.initialize();
  delay(1000);

  pinMode(ledPinTilt, OUTPUT);
  pinMode(ledPinALive, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  pinMode(ledPinVibracion, OUTPUT);
  pinMode(botonPin, INPUT);

  if (mpu.testConnection()) {
  } else {
    while (1);
  }
  tiempo_espera = millis();
  espera = 0;
  Modos = modo_funcionamiento();
  tiempo_prev = millis();
  calibrateOffsets();
}

void loop() {
  unsigned long currentMillis = millis();
  // Leer aceleraciones y velocidades angulares
  mpu.getAcceleration(&ax, &ay, &az);
  mpu.getRotation(&gx, &gy, &gz);

  updateFiltered();

  if (currentMillis - tiempoled >= 1000) {
    digitalWrite(ledPinALive, !digitalRead(ledPinALive));
    tiempoled = currentMillis;
  }
  // Calcular retraso basado en ángulos
  int delayTime = calculateDelay(ang_x, ang_y);
  vibrating_angle(delayTime, currentMillis, Modos);
  delay(50);
}

void updateFiltered() {
  float accel_ang_x_prev;
  float accel_ang_y_prev;
  float accel_ang_z_prev;
  dt = (millis() - tiempo_prev) / 1000.0;
  tiempo_prev = millis();

  float accel_ang_x = atan(ax / sqrt(pow(ay, 2) + pow(az, 2))) * (180.0 / PI);
  float accel_ang_y = atan(ay / sqrt(pow(ax, 2) + pow(az, 2))) * (180.0 / PI);
  float accel_ang_z = atan(sqrt(pow(ax, 2) + pow(ay, 2)) / az ) * (180.0 / PI);

  float accel_ang_x_filtered = pond_flit * accel_ang_x_prev + (1 - pond_flit) * accel_ang_x;
  float accel_ang_y_filtered = pond_flit * accel_ang_y_prev + (1 - pond_flit) * accel_ang_y;
  float accel_ang_z_filtered = pond_flit * accel_ang_z_prev + (1 - pond_flit) * accel_ang_z;

  accel_ang_x_prev = accel_ang_x_filtered;
  accel_ang_y_prev = accel_ang_y_filtered;
  accel_ang_z_prev = accel_ang_z_filtered;

  // Calcular ángulo de rotación con el giroscopio y filtro complementario
  ang_x = pond_ang * (ang_x_prev + (gx / 131.0) * dt) + (1 - pond_ang) * accel_ang_x_filtered;
  ang_y = pond_ang * (ang_y_prev + (gy / 131.0) * dt) + (1 - pond_ang) * accel_ang_y_filtered;
  ang_z = pond_ang * (ang_z_prev + (gz / 131.0) * dt) + (1 - pond_ang) * accel_ang_z_filtered;


  ang_x_prev = ang_x;
  ang_y_prev = ang_y;
  ang_z_prev = ang_z;

  ang_x = ang_x - ang_x_offset;
  ang_y = ang_y - ang_y_offset;
  ang_z = ang_z - ang_z_offset;
}

void calibrateOffsets() {
  long startTime = millis();
  int numReadings = 0;

  float sum_ang_x = 0;
  float sum_ang_y = 0;
  float sum_ang_z = 0;

  Serial.println(F("Calibrando..."));

  while (millis() - startTime < calibrationTime) {
    mpu.getAcceleration(&ax, &ay, &az);
    mpu.getRotation(&gx, &gy, &gz);

    updateFiltered();

    sum_ang_x += ang_x;
    sum_ang_y += ang_y;
    sum_ang_z += ang_z;

    numReadings++;
    delay(20);
  }

  ang_x_offset = sum_ang_x / numReadings;
  ang_y_offset = sum_ang_y / numReadings;
  ang_z_offset = sum_ang_z / numReadings;

  ang_x = ang_x - ang_x_offset;
  ang_y = ang_y - ang_y_offset;
  ang_z = ang_z - ang_z_offset;

  Serial.println(F("Calibración completa"));
  tone(buzzerPin, frec_inictalizate);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, frec_inictalizate);
  delay(100);
  noTone(buzzerPin);
}

int calculateDelay(float ang_x, float ang_y) {
  ang_x = constrain(ang_x, -90, 90);
  ang_y = constrain(ang_y, -90, 90);

  float magnitude = sqrt(ang_x * ang_x + ang_y * ang_y);

  int delayTime = map(magnitude, 0, 8, 100, 10);

  return constrain(delayTime, 0, 200);
}

void vibrating_angle(int delayTime, unsigned long currentMillis, int Modos) {
  const int vibrationThreshold = 400;
  switch (Modos) {
    case 1:
      if (currentMillis - tiempovibracion >= delayTime) {
        tiempovibracion = currentMillis;
        digitalWrite(ledPinVibracion, !digitalRead(ledPinVibracion));
        digitalWrite(ledPinTilt, LOW);
        if (digitalRead(ledPinVibracion)) {
          tone(buzzerPin, frequency1);
        } else {
          noTone(buzzerPin);
        }
      }
      break;

    case 2:
      if (currentMillis - tiempoinclinacion >= delayTime) {
        tiempoinclinacion = currentMillis;
        digitalWrite(ledPinTilt, !digitalRead(ledPinTilt));
        digitalWrite(ledPinVibracion, LOW);
        if (digitalRead(ledPinTilt)) {
          tone(buzzerPin, frequency2);
        } else {
          noTone(buzzerPin);
        }
      }
      break;

    case 3:
      isVibrating = abs(gx) > vibrationThreshold || abs(gy) > vibrationThreshold || abs(gz) > vibrationThreshold;

      // Lógica para manejar vibración
      if (isVibrating && (abs(ang_x) < 0.5 && abs(ang_y) < 0.5)) { // Vibración pura (teniamos 15°)
        if (currentMillis - tiempovibracion >= delayTime) {
          tiempovibracion = currentMillis;
          digitalWrite(ledPinVibracion, !digitalRead(ledPinVibracion));
          digitalWrite(ledPinTilt, LOW);
          if (digitalRead(ledPinVibracion)) {
            tone(buzzerPin, frequency1);
          } else {
            noTone(buzzerPin);
          }
        }
      } else if ((abs(ang_x) > 1 || abs(ang_y) > 1)) {
        if (currentMillis - tiempoinclinacion >= delayTime) {
          tiempoinclinacion = currentMillis;
          digitalWrite(ledPinTilt, !digitalRead(ledPinTilt));
          digitalWrite(ledPinVibracion, LOW);
          if (digitalRead(ledPinTilt)) {
            tone(buzzerPin, frequency2);
          } else {
            noTone(buzzerPin);
          }
        }
      }
      break;
    default:
      noTone(buzzerPin);
      digitalWrite(ledPinTilt, LOW);
      digitalWrite(ledPinVibracion, LOW);
  }
}


int modo_funcionamiento() {
  int modos = 0;
  while (espera == 0) {
    if (digitalRead(botonPin) == HIGH) {
      delay(200);
      if (digitalRead(botonPin) == LOW) {
        modo++;
        tiempo_espera = millis();
      }
    } if (modo > 0) {
      switch (modo) {
        case 1:
          modos = 1;
          break;
        case 2:
          modos = 2;
          break;
        case 3:
          modos = 3;
          break;
        default:
          modo = 1;
      }
      if (millis() - tiempo_espera >= tiempo_esperado) {
        espera ++;
      }
    }
  }
  return modos;
}
